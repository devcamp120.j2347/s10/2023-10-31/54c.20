import models.MovablePoint;

public class App {
    public static void main(String[] args) throws Exception {
        MovablePoint p1 = new MovablePoint(0, 0);
        MovablePoint p2 = new MovablePoint(10, 10);
        
        System.out.println(p1.toString());
        System.out.println(p2.toString());

        p1.moveUp();
        p2.moveUp();

        System.out.println(p1.toString());
        System.out.println(p2.toString());

        p1.moveDown();
        p2.moveDown();

        System.out.println(p1.toString());
        System.out.println(p2.toString());

        p1.moveLeft();
        p2.moveLeft();

        System.out.println(p1.toString());
        System.out.println(p2.toString());
        
        p1.moveRight();
        p2.moveRight();

        System.out.println(p1.toString());
        System.out.println(p2.toString());

    }
}
